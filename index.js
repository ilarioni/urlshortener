require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();

// Basic Configuration
const port = process.env.PORT || 3000;

app.use(cors());
// 111
app.use(express.urlencoded({ extended: true }))
//

app.use('/public', express.static(`${process.cwd()}/public`));

app.get('/', function(req, res) {
  res.sendFile(process.cwd() + '/views/index.html');
});

// Your first API endpoint
app.get('/api/hello', function(req, res) {
  res.json({ greeting: 'hello API' });
});

// 111 newly added logic
const originalUrls = []
const shortUrls = []

app.post('/api/shorturl', (req, res) => {
  // url
  const url = req.body.url 

  // if(originalUrls.find((originalUrl) => originalUrl === url))
  const foundIndex = originalUrls.indexOf(url)

  if(!url.includes("https://") && !url.includes("http://")) {
    return res.json({
      error: 'invalid url'
    })
  }
  
  if(foundIndex < 0) {
    originalUrls.push(url)
    shortUrls.push(shortUrls.length)

    return res.json({
      original_url: url,
      short_url: shortUrls.length - 1
      
    })
  }

  return res.json({
    original_url: url,
    short_url: shortUrls[foundIndex]
  })
  // res.json(req.body.url)
})

// reference end result: {"original_url":"https://www.google.com","short_url":1}

app.get("/api/shorturl/:shorturl", (req, res) => {
  const shorturl = parseInt(req.params.shorturl)
  const foundIndex = shortUrls.indexOf(shorturl)

  if(foundIndex < 0) {
    return res.json({
      "error": "No short URL found for the given input"
    })
  }

  res.redirect(originalUrls[foundIndex])

  
  
})
//

app.listen(port, function() {
  console.log(`Listening on port ${port}`);
});
